import { App, Directive, defineAsyncComponent } from "vue";
import directive from './directive'
 




// 批量注册组件
let module: any = import.meta.glob("../components/**/*.vue");
const plugin = (app:App) => {
  for (const key in directive) {
     
    app.directive(key,directive[key] as Directive)
    
  }
        


  return {
    install(app: App) {
      
      for (const key in module) {
        const moduleName = key.match(/.*\/(.+).vue$/)![1];
        const moduleConent = module[key];
        app.component(moduleName, defineAsyncComponent(moduleConent));
      }
    }
     
  };
};

export default plugin;
