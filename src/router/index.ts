import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import { useInfoStore } from "@/store/module/user";
//引入nprogress
import NProgress from "nprogress"; // 进度条
import "nprogress/nprogress.css"; //这个样式必须引入
// 简单配置
import { tarbars } from "@/util/index";
NProgress.configure({ easing: "ease", speed: 500, showSpinner: false });

const routes: Array<RouteRecordRaw> = [
  {
    path: "/login",
    name: "login",
    component: () =>
      import(
        /* webpackChunkName: 'ImportFuncDemo' */ "./../views/login/index.vue"
      ),
  },
  {
    path: "/",
    name: "page",
    redirect: "/home",
    component: () =>
      import(
        /* webpackChunkName: 'ImportFuncDemo' */ "./../views/compont/index.vue"
      ),
    children: [
      {
        path: "/home",
        name: "home",
        component: () =>
          import(
            /* webpackChunkName: 'ImportFuncDemo' */ "./../views/home/index.vue"
          ),
        meta: {
          title: "首页",
          icon: "icon-home",
        },
      },
      {
        path: "/liveVideo",
        name: "liveVideo",
        component: () =>
          import(
            /* webpackChunkName: 'ImportFuncDemo' */ "./../views/blob/index.vue"
          ),
        meta: {
          title: "我的博客",
          icon: "icon-liuyan1",
        },
      },
      {
        path: "/healthy",
        name: "healthy",

        meta: {
          title: "生活",
          icon: "icon-jiankang",
        },
        children: [
          {
            path: "/healthy/weight",
            name: "weight",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/hentlh/healthy/index.vue"
              ),
            meta: {
              title: "健康生活",
               
            },
          },
        ],
      },
      {
        path: "/warningManagement",
        name: "warningManagement",

        meta: {
          title: "标签管理",
          icon: "icon-liuyan1",
        },
        children: [
          {
            path: "/warningManagement/edgeBoxWarning",
            name: "edgeBoxWarning",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/tagType/type/index.vue"
              ),
            meta: {
              title: "文章分类",
              icon: "icon-liuyan1",
            },
          },
          {
            path: "/warningManagement/radarWarning",
            name: "radarWarning",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/tagType/tag/index.vue"
              ),
            meta: {
              title: "标签列表",
              icon: "icon-liuyan1",
            },
          },
        ],
      },

      {
        path: "/deviceWarn",
        name: "deviceWarn",
        // component: () =>
        //   import(
        //     /* webpackChunkName: 'ImportFuncDemo' */ "./../views/deviceWarn/index.vue"
        //   ),
        meta: {
          title: "文章管理",
          icon: "icon-liuyan1",
        },
        redirect: "/deviceWarn/addaRtic",
        children: [
          {
            path: "/deviceWarn/addaRtic",
            name: "addaRtic",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/deviceWarn/router/add/index.vue"
              ),
            meta: {
              title: "添加文章",
              icon: "icon-liuyan1",
            },
          },
          {
            path: "/deviceWarn/listaRtic",
            name: "listaRtic",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/deviceWarn/router/list/index.vue"
              ),
            meta: {
              title: "文章列表",
              icon: "icon-liuyan1",
            },
          },
        ],
      },
      {
        path: "/systemManagement",
        name: "systemManagement",
        redirect: "/systemManagement/Account",
        // component: () =>
        //   import(
        //     /* webpackChunkName: 'ImportFuncDemo' */ "./../views/systemManagement/index.vue"
        //   ),
        meta: {
          title: "系统管理",
          icon: "icon-liuyan1",
        },
        children: [
          {
            path: "/systemManagement/Account",
            name: "Account",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/systemManagement/Account/index.vue"
              ),
            meta: {
              title: "账号管理",
            },
          },
          {
            path: "/systemManagement/friendlist",
            name: "friendlist",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/systemManagement/friendlist/index.vue"
              ),
            meta: {
              title: "友链管理",
            },
          },
          {
            path: "/systemManagement/network",
            name: "network",

            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/systemManagement/setting/index.vue"
              ),
            meta: {
              title: "文件管理",
            },
          },

          {
            path: "/systemManagement/history",
            name: "history",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/compont/threeComponents.vue"
              ),
            redirect: "/systemManagement/history/operation",
            meta: {
              title: "日志管理",
            },
            children: [
              {
                path: "/systemManagement/history/operation",
                name: "historyoperation",
                component: () =>
                  import(
                    /* webpackChunkName: 'ImportFuncDemo' */ "./../views/systemManagement/history/router/operation/index.vue"
                  ),
                meta: {
                  title: "操作日志",
                },
              },
              {
                path: "/systemManagement/history/working",
                name: "historyworking",
                component: () =>
                  import(
                    /* webpackChunkName: 'ImportFuncDemo' */ "./../views/systemManagement/history/router/working/index.vue"
                  ),
                meta: {
                  title: "运行日志",
                },
              },
            ],
          },
        ],
      },
      // {
      //   path: "/subsystemsManagement",
      //   name: "subsystemsManagement",
      //   meta: {
      //     title: "子系统管理",
      //     icon: "icon-liuyan1",
      //   },

      //   children: [
      //     {
      //       path: "/subsystemsManagement/energy",
      //       name: "energy",
      //       redirect: "/subsystemsManagement/energy/curx",
      //       component: () =>
      //         import(
      //           /* webpackChunkName: 'ImportFuncDemo' */ "./../views/compont/threeComponents.vue"
      //         ),
      //       meta: {
      //         title: "能源系统",
      //       },
      //       children: [
      //         {
      //           path: "/subsystemsManagement/energy/curx",
      //           name: "curx",
      //           component: () =>
      //             import(
      //               /* webpackChunkName: 'ImportFuncDemo' */ "./../views/subsystemsManagement/energy/router/curx.vue"
      //             ),
      //           meta: {
      //             title: "简历模板",
      //           },
      //         },
      //         {
      //           path: "/subsystemsManagement/energy/control",
      //           name: "control",
      //           component: () =>
      //             import(
      //               /* webpackChunkName: 'ImportFuncDemo' */ "./../views/subsystemsManagement/energy/router/control.vue"
      //             ),
      //           meta: {
      //             title: "控制指令",
      //           },
      //         },
      //         {
      //           path: "/subsystemsManagement/energy/system",
      //           name: "system",
      //           component: () =>
      //             import(
      //               /* webpackChunkName: 'ImportFuncDemo' */ "./../views/subsystemsManagement/energy/router/system.vue"
      //             ),
      //           meta: {
      //             title: "系统设置",
      //           },
      //         },
      //       ],
      //     },
      //     {
      //       path: "/subsystemsManagement/Gimbal",
      //       name: "Gimbal",
      //       component: () =>
      //         import(
      //           /* webpackChunkName: 'ImportFuncDemo' */ "./../views/subsystemsManagement/Gimbal/index.vue"
      //         ),
      //       meta: {
      //         title: "云台系统",
      //       },
      //     },
      //     {
      //       path: "/subsystemsManagement/SystemTesting",
      //       name: "SystemTesting",
      //       component: () =>
      //         import(
      //           /* webpackChunkName: 'ImportFuncDemo' */ "./../views/subsystemsManagement/SystemTesting/index.vue"
      //         ),
      //       meta: {
      //         title: "系统测试",
      //       },
      //     },
      //   ],
      // },
      {
        path: "/config",
        name: "config",
        meta: {
          title: "参数配置",
          icon: "icon-liuyan1",
        },
        // component: () =>
        // import(
        //   /* webpackChunkName: 'ImportFuncDemo' */ "./../views/config/index.vue"
        // ),
        children: [
          {
            path: "/config/setting",
            name: "setting",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/config/index.vue"
              ),
            meta: {
              title: "参数配置",
            },
          },
          {
            path: "/config/about",
            name: "about",
            component: () =>
              import(
                /* webpackChunkName: 'ImportFuncDemo' */ "./../views/config/about.vue"
              ),
            meta: {
              title: "我的资料",
            },
          },
        ],
      },
    ],
  },
  {
    path: "/404",
    name: "PageNotExist",
    component: () => import("./../components/404.vue"),
  },
  {
    path: "/:catchAll(.*)", // 不识别的path自动匹配404
    redirect: "/404",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});
// 当路由请求之前
router.beforeEach((to, from, next) => {
  const store = useInfoStore();
  NProgress.inc(0.2);
  // 每次切换页面时，调用进度条
  NProgress.start();
  // 有token
  if (store.$state.token) {
    console.log(to.path);

    // 直接放行
    if (to.path === "/login") {
      // 直接放行
      next("/");
    } else {
      next();
    }
  } else {
    // 否则是没有
    // 如果去的是登录页
    if (to.path === "/login") {
      // 直接放行
      next();
    } else {
      next("/login");
    }
  }
});
// 当路由请求之后：关闭进度条
router.afterEach((to, from, failure) => {
  // 在即将进入新的页面组件前，关闭掉进度条
  tarbars(to, from, failure); //收集路由数据
  NProgress.done();
});

// app.use(router);
export default router;
