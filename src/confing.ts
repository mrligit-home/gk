const BaseConfig = {
  BaseUrl: import.meta.env.VITE_DATA_BASE_URL,
  Timeout: 60000,
  singlKey:'pass'
};

export default BaseConfig;
