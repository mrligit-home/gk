export const imgUpload = (file) => {
  return new Promise<any>((resolve, reject) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.onerror = function (err) {
      reject(err);
    };
  });
};
/**
 * base64图片转file的方法（base64图片, 设置生成file的文件名）
 * @param {*} base64
 * @param {*} fileName
 * @returns
 */
export const base64ToFile=(base64, fileName)=> {
  let data = base64.split(",");
  let type = data[0].match(/:(.*?);/)[1];
  let suffix = type.split("/")[1];
  const bstr = window.atob(data[1]);
  let n = bstr.length;
  const u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  // 利用构造函数创建File文件对象
  const file = new File([u8arr], `${fileName}.${suffix}`, {
    type: type,
  });
  return file;
}
// base64转成文件下载（base64—> blob）
export const downMailDataEncrypt = (base64data) => {
  var blob = dataURLtoBlob(base64data);

  const a = document.createElement("a");
  const objectUrl = URL.createObjectURL(blob);
  a.setAttribute("href", objectUrl);
  a.setAttribute("download", "test.txt");
  a.click();
  URL.revokeObjectURL(a.href); // 释放url
  // base64转blob
  function dataURLtoBlob(dataurl) {
    var arr = dataurl.split(","),
      // mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[0]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], { type: "vkd" });
  }
};

// 上传文件File文件对象转成二进制流
export const fileUpload = (e) => {
  let file = e.target.files[0];
  // 前端转换成二进制流
  let that = this;
  var reader = new FileReader();
  reader.onload = function (e) {
    var arrayBuffer = e.target.result;
    that.fileData = arrayBuffer;
  };
  reader.readAsArrayBuffer(file);
};

// 应用场景：响应文件流前端js通过blob生成下载链接
export const blodLoad = (res) => {
  // 文件流res下载
  // 流转 blob
  var blob = new window.Blob([res], { type: "application/x-msdownload" });

  const a = document.createElement("a");
  const objectUrl = URL.createObjectURL(blob);
  a.setAttribute("href", objectUrl);
  a.setAttribute("download", "test.txt");
  a.click();
  URL.revokeObjectURL(a.href); // 释放url
};

export const setUrl = (url) => {
  const regex = /^(http|https):\/\//;
 

  if (regex.test(url)) {
    return url;
  }
  return import.meta.env.VITE_DATA_BASE_URL + url;
};


export const getDataType=(data)=> {
  if (Array.isArray(data)) {
    return 'array';
  } else if (typeof data === 'object' && data !== null) {
    return 'object';
  } else if (typeof data === 'string') {
    return 'string';
  } else if (typeof data === 'number') {
    return 'number';
  } else if (typeof data === 'boolean') {
    return 'boolean';
  } else if (typeof data === 'function') {
    return 'function';
  } else if (data instanceof Date) {
    return 'date';
  } else if (typeof data === 'undefined') {
    return 'undefined';
  } else if (data === null) {
    return 'null';
  } else {
    return 'unknown';
  }
}

 