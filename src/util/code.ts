const Code_list = {
  ADD: "AddData",   //增加
  EDIT: "EDIT",  //修改
  DELETE: "DeleteData",    //删除
  ADDandEDIT: "ADDandEDIT", //增加修改
  WATCH: "WATCH",   //查看
  SEARCH: "SearchData", //搜索
  RESET: "ResetSearch", //重置
  SAVE: "SaveData",     //保存
  DROPDOW: "DropdownData",   //下拉
  OTHER:'OTHER'   //其他，自定义
};

export default Code_list;
