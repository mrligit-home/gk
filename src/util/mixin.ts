import http from "@/request";
import { resetObj } from "@/util/index";
import { message } from "@/util";
import code from "@/util/code";
import {
  useAttrs,
  reactive,
  ref,
  onMounted,
  onBeforeMount,
  toRefs,
  onBeforeUnmount,
  h,
} from "vue";
import { ElMessageBox } from "element-plus";
let arrMixin = (targetObj, orgin, objectlis = {}) => {
  for (let i = 0; i < orgin.length; i++) {
    const iterator = orgin[i];
    if (typeof iterator != "object") {
      targetObj.push(iterator);
    } else {
      if (Array.isArray(iterator)) {
        arrMixin(targetObj, iterator);
      } else {
        mixinFn((targetObj[i] ??= {}), iterator);
      }
    }
  }
};
var mixinFn = (targetObj: Object = {}, orgin: Object) => {
  for (const key in orgin) {
    if (!(key in targetObj) || typeof orgin[key] != "object") {
      targetObj[key] = orgin[key];
    } else {
      if (Array.isArray(orgin[key])) {
        arrMixin(targetObj[key], orgin[key]);
      } else {
        mixinFn(targetObj[key], orgin[key]);
      }
    }
  }
};
const Mixin = () => {
  const gridRef = ref();
  const attrs = useAttrs();
  let inforConfig = null;
  const listData = reactive({
    data: [],
    total: 0,
  });
  const el_table_Ref = ref(); //tableRef
  const Single_Choice_data = ref([]); //单选数据表
  const Choice_data = ref([]); //复选框数据表

  const gridOptions = {
    service: true,
    tableConfig: {
      height: "100%",
      border: true,
      highlightCurrentRow: true,
      size: attrs.size,
      showOverflowTooltip: true,
      emptyText: "暂无数据",
    },

    columnConfig: {
      align: "center",
      headerAlign: "center",
    },
  };

  const watchData = reactive({
    pagerConfig: {
      pageSize: 5,
      page: 1,
      pageSizes: [5, 10, 15, 20, 50],
      layout: "sizes, prev, pager, next,jumper, ->, total",
      pageFirst: true,
    },
    Option: {
      method: "get",
      params: {},
    },
    SearchForm: {},
    ElMessageConfig: {
      title: "确认删除？删除后数据将不存在",
    },
  });

  mixinFn(gridOptions, attrs.gridOptions);
  mixinFn(watchData, attrs.watchData);

  const { pagerConfig, Option, SearchConfig, deleteConfig } = toRefs(watchData); //响应式解构

  const fetch_data = () => {
    Option.value.params.limit = pagerConfig.value.pageSize;
    Option.value.params.page = pagerConfig.value.page;

    return new Promise<void>(async (resolve, reject) => {
      let { data } = await http(Option.value);
      listData.data.forEach((element) => {
        element["status"] = false;
      });
      listData.data = data.data.data;
      listData.total = data.count;
      resolve();
    });
  };
  const SearchData = () => {
    Option.value.params.filter = {};

    Object.assign(Option.value.params.filter, attrs.watchData.SearchForm);

    if (SearchConfig.value) {
      SearchConfig.value.callback(Option.value, (conf) => {
        //  console.log('我的回调');
        Option.value = conf;
      });
    }

    for (const key in Option.value.params.filter) {
      let val = Option.value.params.filter[key];
      if (Object.hasOwn(Option.value.params.filter, key)) {
        if (Array.isArray(val) && val.length == 0) {
          delete Option.value.params.filter[key];
        }
        if (val instanceof Object && !Object.keys(val).length) {
          delete Option.value.params.filter[key];
        }
        if (typeof val != "object" && !val) {
          delete Option.value.params.filter[key];
        }
      }
    }

    return fetch_data();
  };
  const DeleteData = (option = { data: [] }) => {
    let { data = [] } = option;
    const regex = /delete|post/i;

    let Option = {
      method: "delete",
      data: {},
      params: {},
    };
    Object.assign(Option, gridOptions.BaseConfig?.deleteConfig.Option);
    if (!Option?.url) return message("error", "url不存在！");

    Choice_data.value.push(...data);

    if (!Choice_data.value.length)
      return message("error", "至少选择一条数据！");

    return new Promise((resolve, reject) => {
      ElMessageBox.confirm(
        watchData.ElMessageConfig.title,
        (watchData.ElMessageConfig.Confirm ??= "提示"),
        {
          distinguishCancelAndClose: true,
          confirmButtonText: (watchData.ElMessageConfig.confirmButtonText ??=
            "确认"),
          cancelButtonText: (watchData.ElMessageConfig.cancelButtonText ??=
            "取消"),
        }
      )
        .then(() => {
          let arrayDelete = Choice_data.value.map(
            (item) => item.id || item._id
          );
          if (regex.test(Option.method)) {
            Option.data[gridOptions.BaseConfig?.deleteConfig.key] = Array.from(
              new Set(arrayDelete)
            );
          }
          if (deleteConfig.value) {
            deleteConfig.value?.callback(Option, (conf) => {
              Option = conf;
            });
          }

          http(Option)
            .then((res) => {
              SearchData().then((res) => {
                Choice_data.value = [];
              });

              resolve(true);
            })
            .catch((e) => {});
        })
        .catch(() => {
          Choice_data.value = [];
          if (el_table_Ref.value) {
            el_table_Ref.value.clearSelection();
          }
          resolve(false);
        });
    });
  };

  const SaveData = (e) => {};

  const ResetSearch = () => {
    pagerConfig.value.page = 1;

    if (Object.keys(inforConfig).length) {
      resetObj(attrs.watchData.SearchForm, inforConfig);
    } else {
      resetObj(attrs.watchData.SearchForm);
    }

    SearchData();
  };

  onBeforeMount(() => {
    inforConfig = JSON.parse(JSON.stringify(attrs.watchData.SearchForm));
  });
  onMounted(() => {});
  onBeforeUnmount(() => {
    inforConfig = null;
  });
  return {
    SearchData,
    DeleteData,
    SaveData,
    ResetSearch,
    Single_Choice_data,
    Choice_data,
    fetch_data,
    Option,
    listData,
    gridOptions,
    gridRef,
    el_table_Ref,
    pagerConfig,
  };
};

export default Mixin;

export const formConfig = () => {
  const attrs = useAttrs();

  let conf = {
    statusIcon: true,
    disabled: false,
    labelWidth: "auto",
    showMessage: true,
    size: "default",
    labelPosition: "left",
    hideRequiredAsterisk: false,
  };
  const watchForm = reactive({});

  mixinFn(conf, attrs.formConf);
  mixinFn(watchForm, attrs.watchForm);
  let { model, BaseConfig } = toRefs(watchForm);
  console.log(287, conf);

  return {
    conf,
    model,
    BaseConfig,
  };
};
let arrsa = [];

export const dialogConfig = (emit?: any) => {
  const DialogCustom = ref([]);
  const attrs = useAttrs();
  console.log(272, attrs);

  const DialogConfig = reactive({
    operate: [
      //HEAFER  事件
     
    ],
  });

  mixinFn(DialogConfig.operate, attrs.dialogList);
  let obj = {
    beforeClose: (e) => emit("beforeClose", e),
    Cancel: (e) => emit("cancel", e),
    Confirm: (e) => emit("confirm", e),
  };
  for (const iterator of DialogConfig.operate) {
   for (const itera of Object.keys(obj)) {
    if (iterator.config&&!(itera in iterator)) {
      iterator[itera]=obj[itera]
    }
   }
    
  }
  return {
    DialogConfig,
  };
};
