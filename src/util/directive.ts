const controllerMap = new WeakMap<Element, AbortController>();
import { nextTick } from "vue";
import imgNo from "../assets/image/nosa.png";
const directive: Object = {
  outside: {
    //   v-outside:[row]="call"
    mounted(el: any, binding: any, vnode) {
      let flag = false;

      const controller = new AbortController();
      controllerMap.set(el, controller);
      let data = el.getBoundingClientRect();

      window.addEventListener(
        "click",
        (e) => {
          let { pageX, pageY } = e;

          if (
            !(
              pageX > data.x &&
              pageX < data.x + el.offsetWidth &&
              pageY > data.y &&
              pageY < data.y + el.offsetHeight
            )
          ) {
            if (flag) binding.value(binding.arg);

            flag = true;
          }
        },
        { signal: controller.signal }
      );
    },
    beforeUnmount(el, binding, vnode) {
      // 当指令绑定的元素 的父组件销毁前调用。  <简单讲，指令元素的父组件销毁前调用>
      const controller = controllerMap.get(el);
      controller?.abort(); // 移除事件
    },
    unmounted(el, binding, vnode) {
      // 当指令与元素解除绑定且父组件已销毁时调用。
    },
  },
  shake: {
    mounted(el: any, binding: any, vnode) {
      let date = new Date().getTime();
      let curryDate = 0;

      el.onclick = (e) => {
        curryDate = new Date().getTime();

        if (curryDate - date > 500) {
          binding.value.call();
        }
        date = new Date().getTime();
      };
    },
  },
  ImgErr: {
    mounted(el: any, binding: any, vnode) {
      el.onerror = () => {
        el.src =
          "https://gitee.com/api/v5/repos/mrligit-home/blob-static-page/raw/image/nosa.png?access_token=dc8320c78fe70b0151bd16989954fc91";
      };
    },
  },
  foucs: {
    mounted(el: any, binding: any, vnode) {
      el.querySelector("input").focus();

      nextTick(() => {
        el.querySelector("input").value = binding.value.name;

        var file_name = el
          .querySelector("input")
          .value.replace(/(.*\/)*([^.]+).*/gi, "$2");

        el.querySelector("input").setSelectionRange(0, file_name.length);
      });
    },
  },
  butt: {
    mounted(el, binding, vnode, prevNode) {
      console.log(binding);
    },
    beforeUpdate(el, binding, vnode, prevNode) {
      console.log(binding);
    },
    updated() {},
  },
};

export default directive;
