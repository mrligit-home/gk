import { defineAsyncComponent } from "vue";
const formMap = {
 
  // me_check: defineAsyncComponent(() =>
  //   import("@/components/Elcomponent/me_check.vue")
  // ),
  me_datepicker: defineAsyncComponent(() =>
    import("@/components/Elcomponent/me_datepicker.vue")
  ),
  me_input: defineAsyncComponent(() =>
    import("@/components/Elcomponent/me_input.vue")
  ),
  me_picker: defineAsyncComponent(() =>
    import("@/components/Elcomponent/me_picker.vue")
  ),
  me_radio: defineAsyncComponent(() =>
    import("@/components/Elcomponent/me_radio.vue")
  ),
  me_select: defineAsyncComponent(() =>
    import("@/components/Elcomponent/me_select.vue")
  ),
  me_switch: defineAsyncComponent(() =>
    import("@/components/Elcomponent/me_switch.vue")
  ),
  me_update: defineAsyncComponent(() =>
    import("@/components/Elcomponent/me_update.vue")
  ),
  
};

export default formMap;
