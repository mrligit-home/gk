// 批量注册组件
let module: any = import.meta.glob("../components/**/*.vue");
import CryptoJS from "crypto-js";
import Cookies from "js-cookie";
import BaseConfig from "@/confing";
import { ElMessage } from "element-plus";
import {
  App,
  Directive,
  defineAsyncComponent,
  nextTick,
  onMounted,
  onBeforeUnmount,
} from "vue";
import directive from "./directive";
import { ref } from "vue";
const plugin = (app: App) => {
  for (const key in directive) {
    app.directive(key, directive[key] as Directive);
  }

  return {
    install(app: App) {
      for (const key in module) {
        const moduleName = key.match(/.*\/(.+).vue$/)![1];
        const moduleConent = module[key];
        app.component(moduleName, defineAsyncComponent(moduleConent));
      }
    },
  };
};

export default plugin;

export const RequireImg = (path: string) => {
  if (typeof path != "string") throw new Error("类型错误");
  return new URL(`../assets/${path}`, import.meta.url).href;
};

interface message {
  type?: string;
  message?: string;
}
let messageInstance: any;

export const message = (type: string, options: string, config: Object = {}) => {
  const arr = ["success", "warning", "info", "error"];
  let obj: any = {
    ...config,
  };
  return (() => {
    // console.log(63, obj);
    if (messageInstance) messageInstance.close();

    if (!arr.includes(type)) return;
    if (typeof options === "string") {
      obj["message"] = options;
    }
    obj["type"] = type;

    messageInstance = ElMessage(obj);
  })();
};

// 分页历史记录
export class pageRecord {
  list: Array<object>;
  obj: object;
  type: string;
  dom: HTMLElement | any;
  Page: any;
  listData: Array<object>;
  constructor(dom: any) {
    this.list = [];
    this.obj = {};
    this.type = "";
    this.dom = dom;
    this.Page = 0;
    this.listData = [];
  }
  Record(data: any, page: number | string) {
    this.Page = page;

    if (typeof data != "object") throw "接受数组";
    let currageArry = this.list.find((item) => item.page == page);

    if (currageArry) {
      currageArry["data"] = data;
      if (!data.length)
        this.list = this.list.filter((item) => item.page != page);
    } else {
      let obj: any = {};
      obj["page"] = page;
      obj["data"] = data;
      this.list.push(obj);
    }
  }
  pageHistory(Page: any, listDatas: any) {
    if (this.dom) {
      let ARR = this.list.find((item) => item.page == Page);

      if (!ARR) return;

      listDatas.forEach((items) => {
        let checkobj = ARR.data.find((it) => it.id == items.id);

        if (checkobj) {
          nextTick(() => {
            setTimeout(() => {
              this.dom.toggleRowSelection(items, true);
            }, 500);
          });
        }
      });
    } else {
      throw "dom 不存在";
    }
  }
  ListData() {
    let arr = [];
    this.list.map((item) => arr.push(...item.data));
    return arr;
  }
}

export function debounce(func, wait = 1000, immediate) {
  var timeout;
  return function () {
    var context = this;
    var args = arguments;

    if (timeout) clearTimeout(timeout);
    // 是否在某一批事件中首次执行

    if (immediate) {
      var callNow = !timeout;
      timeout = setTimeout(function () {
        timeout = null;
        func.apply(context, args);
        immediate = true;
      }, wait);
      if (callNow) {
        func.apply(context, args);
      }
      immediate = false;
    } else {
      timeout = setTimeout(function () {
        func.apply(context, args);
        immediate = true;
      }, wait);
    }
  };
}

export const passWord = {
  reset: function () {},
  status: function (data, target) {
    Cookies.set("RememberPassword", data, { expires: 30 });
    if (data) {
      for (const key in target) {
        if (Object.hasOwn(target, key)) {
          this.set(key, target[key]);
        }
      }
    } else {
      for (const key in target) {
        if (Object.hasOwn(target, key)) {
          Cookies.remove(key);
        }
      }
    }
  },
  set: function (key, value) {
    // Encrypt 加密
    var cipherText = CryptoJS.AES.encrypt(
      String(value),
      BaseConfig.singlKey
    ).toString();
    console.log(key, cipherText);

    Cookies.set(key, cipherText, { expires: 30 });
  },
  get: function (user) {
    let base = Cookies.get(user);
    if (user == "RememberPassword") return base == "true" ? true : false;
    // Decrypt 解密
    var bytes = CryptoJS.AES.decrypt(base, BaseConfig.singlKey);
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
  },
};

export const EditFn = function (row, ruleForm, keyv = []) {
  let reg = /\.(png|jpe?g|gif|svg)(\?.*)?$/;
  nextTick(() => {
    for (const key in ruleForm) {
      if (keyv.length && row[key] instanceof Array) {
        ruleForm[key] = row[key].map((item) => item.id);
      } else if (row[key] instanceof Object) {
        ruleForm[key] = row[key].id;
      } else if (reg.test(row[key])) {
        ruleForm[key] = import.meta.env.VITE_DATA_BASE_URL + row[key];
      } else {
        ruleForm[key] = row[key];
      }
    }
  });
};

export const getEmpty = function (row, target, file) {
  let obj = {};
  let reg = /\.(png|jpe?g|gif|svg)(\?.*)?$/;
  for (const key in row) {
    if (Object.hasOwn(row, key) && row[key] instanceof Array) {
      let arr = target[key].map((item) => item.id);
      if (arr.toString() != row[key].toString()) {
        obj[key] = row[key];
      }
    } else if (
      Object.hasOwn(row, key) &&
      row[key] instanceof Object &&
      key != file
    ) {
      if (target[key].id != row[key]) {
        obj[key] = row[key];
      }
    } else if (Object.hasOwn(row, key) && reg.test(row[key])) {
      if (
        Object.hasOwn(row, key) &&
        row[key].replace(import.meta.env.VITE_DATA_BASE_URL, "") != target[key]
      ) {
        obj[key] = row[key];
      }
    } else {
      if (Object.hasOwn(row, key) && target[key] != row[key]) {
        obj[key] = row[key];
      }
    }
  }
  return obj;
};

// base64转换blob
export function dataURLToBlob(fileDataURL) {
  let arr = fileDataURL.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}
/**
 * 从base64编码中解析图片信息
 * @param {String} base64
 * @returns {Object}
 */
function parseBase64(base64) {
  let re = new RegExp("data:(?<type>.*?);base64,(?<data>.*)");
  let res = re.exec(base64);

  if (res) {
    return {
      type: res.groups.type,
      ext: res.groups.type.split("/").slice(-1)[0],
      data: res.groups.data,
    };
  }
}

export const dlowang = (src) => {
  let image = new Image();
  let str, arr;
  // 解决跨域 Canvas 污染问题
  image.setAttribute("crossOrigin", "anonymous");
  image.src = src;

  if (src.startsWith("data:image/")) {
    str = new Date().valueOf()+'.'+parseBase64(src)?.ext || "png";
    arr = ["", parseBase64(src)?.ext || "png"];
  } else {
    str = src.match(/\/(\w+\.(?:png|jpg|gif|bmp|jpeg|webp))$/i)[1]; //error
    arr = str.split(".");
  }

  image.onload = function () {
    let canvas = document.createElement("canvas");
    canvas.width = image.width;
    canvas.height = image.height;
    let context = canvas.getContext("2d");
    context?.drawImage(image, 0, 0, image.width, image.height);
    let url = canvas.toDataURL(`image/${arr[1]}`); //得到图片的base64编码数据
    let a = document.createElement("a"); // 生成一个a元素

    // 获取数据量
    const blob = new Blob([dataURLToBlob(url)]);
    // 下载文件
    const objectUrl = URL.createObjectURL(blob);
    a.href = objectUrl;
    a.setAttribute("download", str);
    a.click();
    URL.revokeObjectURL(objectUrl);
    a = null;
    canvas = null;
  };
};

export const ContextMenuFun = (containerRef) => {
  const x = ref(0);
  const y = ref(0);
  const showMenu = ref(false);
  const callback = ref();
  // 事件处理函数
  const close = () => {
    showMenu.value = false;
  };
  const handleContextMenu = (e) => {
    close();
    if (callback.value) callback.value();
    e.preventDefault();
    e.stopPropagation(); // 阻止冒泡
    showMenu.value = true;
    // console.log();

    x.value = e.clientX;
    y.value = e.clientY;
  };

  onMounted(() => {
    // 将事件处理函数传递传入事件中      // 处理 window 的 contextmenu 事件，用来关闭之前打开的菜单
    if (containerRef.value)
      containerRef.value.addEventListener("contextmenu", handleContextMenu);
    window.addEventListener("click", close, true);
    window.addEventListener("contextmenu", close, true);
  });
  onBeforeUnmount(() => {
    // 将事件处理函数传递传入事件中      // 处理 window 的 contextmenu 事件，用来关闭之前打开的菜单

    if (containerRef.value)
      containerRef.value.removeEventListener("contextmenu", handleContextMenu);
    window.removeEventListener("click", close, true);
    window.removeEventListener("contextmenu", close, true);
  });
  return { x, y, showMenu, callback };
};

import { useStore } from "@/store/module/tar";
import { useRoute, useRouter } from "vue-router";
var id = 0;
export const tarbars = (to, from, d) => {
  let obj = {
    path: to.fullPath,
    name: to.meta.title,
    title: to.name,
    params: to.params,
    query: to.query,
    id: id,
  };
  id++;
  const store = useStore();
  let F = store.$state.historyList.find((item) => item.path == to.fullPath);
  if (!F && to.fullPath != "/home" && to.fullPath != "/login") {
    store.$state.historyList.push(obj);
  }
  if (F) F.id = id;
};

export const useTarbarslist = () => {
  const store = useStore();
  const router = useRouter();
  const route = useRoute();
  const removeAll = () => {
    store.$state.historyList = [];
    router.push("/");
  };
  const removeOther = (e) => {
    let obj = store.$state.historyList.find((item) => item.path == e.path);
    store.$state.historyList = [obj];
    if (route.fullPath != e.path) {
      router.push(e.path);
    }
  };
  const removeNow = (e) => {
    if (store.$state.historyList.length == 1) {
      router.push("/");
      store.$state.historyList = [];
    } else {
      let index = store.$state.historyList.findIndex(
        (item) => item.path == e.path
      );
      if (route.fullPath != e.path) {
        store.$state.historyList.splice(index, 1);
        return;
      }
      let obj = store.$state.historyList.find((item) => item.id == e.id - 1);

      if (index > -1) {
        router.push(obj.path);
        store.$state.historyList.splice(index, 1);
      }
    }
  };
  return {
    removeAll,
    removeOther,
    removeNow,
  };
};

export const staticPage = (url) => {
  return `https://gitee.com/api/v5/repos/mrligit-home/blob-static-page/raw/${url}?access_token=dc8320c78fe70b0151bd16989954fc91`;
};
/**
 * 重置一个FORM对像，可设初始值
 * @param allObj FORM对像
 * @param resetObj 初始值对像
 * @return
 */
export function resetObj(allObj, resetObj?: Object) {
  for (let key in allObj) {
    if (allObj.hasOwnProperty(key)) {
      if (allObj[key] && allObj[key].constructor === Array) {
        allObj[key] = [];
      } else if (allObj[key] && allObj[key].constructor === Boolean) {
        allObj[key] = false;
      } else {
        allObj[key] = "";
      }
    }
  }
  if (resetObj) {
    for (let key in resetObj) {
      if (allObj.hasOwnProperty(key)) {
        allObj[key] = resetObj[key];
      }
    }
  }
}
