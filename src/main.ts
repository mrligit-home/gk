import { createApp } from "vue";
import "./style.css";
import "default-passive-events";
import App from "./App.vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "./assets/style/global.scss";
import "./style.css";
import "../src/assets/style/index.scss";
import router from "./router";
import pinia from "./store"; // 引入
import plugin from "@/util/index";
import "virtual:svg-icons-register";
import { useInfoStore } from "@/store/module/user";
// import '@imengyu/vue3-context-menu/lib/vue3-context-menu.css'
// import ContextMenu from '@imengyu/vue3-context-menu'
import zhLocale from "element-plus/dist/locale/zh-cn.mjs";
// 设置中文
 
import { io } from "socket.io-client";
import "@/util/browserPatch";
const app = createApp(App);
const auth = () => {
  const store = useInfoStore();
  return {
    token: store.$state.token,
    id: store.$state.id,
  };
};
const query = () => {
  const store = useInfoStore();

  return {
    id: "store.$state.id",
  };
};
app
  .use(ElementPlus, {
    locale: zhLocale,
  })
  .use(router)
  .use(pinia)
 
  .use(plugin(app));
let protocol = "ws://";
if (location.protocol == "https:") {
  protocol = "wss://";
}
 

app.config.globalProperties.$socket = io(
  import.meta.env.VITE_DATA_BASE_URL.replace(/^https?:\/\//, protocol),
  {
    transports: ["websocket", "polling"],
    // query:{},
    auth: (cb) => cb(auth()),
  }
);
app.mount("#app");
