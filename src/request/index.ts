import BaseConfig from "@/confing";
import axios from "axios";
import { useInfoStore } from "@/store/module/user";
import router from '@/router/index'
import qs from 'qs'
import { message } from "@/util";

const config = {
  baseURL: BaseConfig.BaseUrl,
  timeout: BaseConfig.Timeout,
  
};

const controller = new AbortController();
const http = axios.create(config);

//  request拦截器
http.interceptors.request.use(
  (config) => {
    const store = useInfoStore();
    let { method, headers } = config;
 
      
    // if (method == "post") {
    //   config.headers["Content-Type"] =
    //     "application/x-www-form-urlencoded;charset=UTF-8";
    // }
    if (method === 'get') {
      // 如果是get请求，且params是数组类型如arr=[1,2]，则转换成arr=1&arr=2
      config.paramsSerializer = function(params) {
        return qs.stringify(params, { arrayFormat: 'indices' })
      }
    }
    config.signal = controller.signal;
    config.headers["Authorization"] =store.$state.token;

    return config;
  },
  (error) => {
 
    return ErrorFun(error);
  }
);
//  respone拦截器
http.interceptors.response.use(
  (response) => {
   
    
     if( response.config.method!='get'&&response.status!=204){
      console.log(42,response.config.method);
      message('success',response.data.msg)
    }
    return response;
  },
  (error) => {
    return ErrorFun(error);
  }
);
// axios.all

const ErrorFun = (err: Error) => {
  
  // console.log(599,err.response.data.code);
  let code=err.response.data.code
  
  
  if(!(err.config.method=='get')){
    message('error',err.response.data.msg??err.message)
  }
  if(code==401){
    const store = useInfoStore();
    store.$reset()
    router.replace('/login')
  }
  return Promise.reject(err);
};

http["all"] = (params: Array<object>) => {
  let apiList: any = [];
  if (!(params instanceof Array)) throw Error("数据类型错误，应为数组");
  for (const iterator of params) {
    apiList.push(http(iterator));
  }
  return new Promise<void>((resolve, reject) => {
    axios
      .all(apiList)
      .then(
        axios.spread((...list: any) => {
          resolve(list);
        })
      )
      .catch((e) => {
        ErrorFun(e);
      });
  });
};
export default http;
