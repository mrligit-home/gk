import http from "@/request";

/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function login(data?: any) {
  return http({
    method: "post",
    url: "/v1/login",
    data,
  });
}
export function logout(data?: any) {
  return http({
    method: "post",
    url: "/v1/logout",
    data,
  });
}
export function svg(params?: any) {
  return http({
    method: "get",
    url: "/v1/svg",
    params,
  });
}
export function addtag(data?: any) {
  return http({
    method: "post",
    url: "/v1/tag",
    data,
  });
}
export function patchtag(data?: any) {
  return http({
    method: "patch",
    url: "/v1/tag",
    data,
  });
}
 
export function addarType(data?: any) {
  return http({
    method: "post",
    url: "/v1/typelist",
    data,
  });
}
export function patcharType(data?: any) {
  return http({
    method: "patch",
    url: "/v1/typelist",
    data,
  });
}
// 单个图片文件上传
export function filupdate(data?: any) {
  return http({
    method: "post",
    url: "/v1/update/file",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data,
  });
}
// 批量图片文件上传
export function filupdates(data?: any) {
  return http({
    method: "post",
    url: "/v1/update/files",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data,
  });
}

export function gettag(params?: any) {
  return http({
    method: "get",
    url: "/v1/tag",
    params,
  });
}
 
export function getType(params?: any) {
  return http({
    method: "get",
    url: "/v1/typelist",
    params,
  });
}

export function addArtic_singl(data?: any) {
  return http({
    method: "post",
    url: "/v1/articlist/add-single",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data,
  });
}

export function addArtic_singl_patch(data?: any) {
  return http({
    method: "patch",
    url: "/v1/articlist/add-single",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data,
  });
}

// 图片删除
export function deletePhoto(data?: any) {
  return http({
    method: "delete",
    url: "/v1/update/files",
    data,
  });
}


// 单个文章
export function singleArtic(params?: any) {
  return http({
    method: "get",
    url: "/v1/articlist/single",
    params,
  });
}

// 文件
export function list(params?: any) {
  return http({
    method: "get",
    url: "/v1/file/list",
    params,
  });
}
// 文件
export function postlist(data:any,params?: any) {
  return http({
    method: "post",
    url: "/v1/file/list",
    data,
    params,
  });
}
// 邮箱
export function Email(data?: any) {
  return http({
    method: "post",
    url: "/email/mag",
    data,
  });
}

// 退出  
export function outline(data?: any) {
  return http({
    method: "post",
    url: "/v1/user/outline",
    data,
  });
}

// 友链列表
export function getfriend(params?: any) {
  return http({
    method: "get",
    url: "/v1/user/list",
    params,
  });
}
// 添加友链
export function addfriend(data?: any) {
  return http({
    method: "post",
    url: "/v1/friend/owm",
    data,
  });
}
// 表情列表
export function emigy(params?: any) {
  return http({
    method: "get",
    url: "/v1/emingy/list",
    params,
  });
}

export function messagePhoto(data?: any,params?:any) {
  return http({
    method: "post",
    url: "/img/list",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data,params
  });
}


export function configData(data?: any,params?:any) {
  return http({
    method: "post",
    url: "/v1/url/config/all",
     
    data,params
  });
}


export function configdata(params?: any) {
  return http({
    method: "get",
    url: "/v1/config",
    params,
  });
}

export function history(params?: any) {
  return http({
    method: "get",
    url: "/v1/history/list",
    params,
  });
}
//历史消息
export function messhistory(params?: any) {
  return http({
    method: "get",
    url: "/v1/message/list",
    params,
  });
}
//历史消息
export function delmesshistory(data?: any) {
  return http({
    method: "delete",
    url: "/v1/message/list",
    data,
  });
}
// 首页
export function gethome(params?: any) {
  return http({
    method: "get",
    url: "/v1/home/all/aggre",
    params,
  });
}
// 关于
export function AboutMe(data?: any) {
  return http({
    method: "post",
    url: "/v1/about/me",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data,
  });
} 

export function getMapList(params?: any) {
  return http({
    method: "get",
    url: "/v1/getMapList",
    params,
  });
}