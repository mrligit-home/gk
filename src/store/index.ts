import piniaPluginPersist from 'pinia-plugin-persistedstate'
import { createPinia } from 'pinia'
const pinia = createPinia()

pinia.use(piniaPluginPersist)

export default pinia
