import { defineStore } from "pinia";
import { configData, configdata } from "@/Api/index";
export const useInfoStore = defineStore("info", {
  state: () => {
    return {
      state: false,
      token: "",
      id: "",
      user: "",
      messcount:0,
      config: {
        browser_title: "",
        browser_icon: "",
        browser_logo: "",
        Blog_title: "",
        Blog_icon: "",
        Blog_logo: "",
        Blog_public: "",
        sorcet_title: "",
        sorcet_icon: "",
      },
      historyList: [],
    };
  },
  actions: {
    async getConfig() {
      let arr = [
        "browser_icon",
        "browser_logo",
        "Blog_icon",
        "Blog_logo",
        "sorcet_icon",
      ];
      let { data } = await configdata({ id: this.id });
      const regex = /^https?:\/\//;
      this.config = data.data.data;
      for (const key in this.config) {
        if (arr.includes(key)) {
          if (!regex.test(this.config[key])) {
            this.config[key] =
              import.meta.env.VITE_DATA_BASE_URL + this.config[key];
          }
        }
      }

      if (this.config.browser_title) {
        document.title = this.config.browser_title;
      }

      this.config.browser_icon;
     

      if (this.config.browser_icon) {
        let $favicon = document.querySelector('link[rel="icon"]');
        if ($favicon !== null) {
          $favicon.href = this.config.browser_icon;
        } else {
          $favicon = document.createElement("link");
          $favicon.rel = "icon";
          $favicon.href = this.config.browser_icon;
          document.head.appendChild($favicon);
        }
      }
    },
    async setConfig() {
      await configData(this.config, { file: "icon", id: this.id });
      await this.getConfig();
    },
  },
  // 持久化配置：若仅配置 true，则全部存储
  // persist: true,
  // 注意这里报错 // 没有与此调用匹配的重载
  persist: {
    key: "user",
    storage: localStorage,
    paths: ["token", "state", "id", "user", "config"],
  },
});
