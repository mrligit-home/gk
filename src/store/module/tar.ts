import { defineStore } from "pinia";
export const useStore = defineStore("info", {
  state: () => {
    return {
      historyList: [],
      keepAliveName:[]
    };
  },
  actions: {},
  // 持久化配置：若仅配置 true，则全部存储
  // persist: true,
  // 注意这里报错 // 没有与此调用匹配的重载
  persist: {
    key: "user",
    storage: localStorage,
    paths: [],
  },
});
