import { defineConfig } from "vite";
import { fileURLToPath, URL } from "node:url";
import vue from "@vitejs/plugin-vue";
import { autoComplete, Plugin as importToCDN } from "vite-plugin-cdn-import";
import compression from "vite-plugin-compression";
import { visualizer } from 'rollup-plugin-visualizer';
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";
 

export default defineConfig({
  plugins: [
    createSvgIconsPlugin({
      iconDirs: [fileURLToPath(new URL("./src/assets/svg", import.meta.url))],
      symbolId: "icon-[name]",
      inject: "body-last",
      customDomId: "__svg__icons__dom__",
    }),
    vue(),
     
    visualizer({
      emitFile: true,
      filename: 'analysis-chart.html', // 分析图生成的文件名
      open:true // 如果存在本地服务端口，将在打包后自动展示
    }),
    importToCDN({
      // prodUrl: 'https://cdn.jsdelivr.net/npm/{name}@{version}/{path}',
      modules: [
        {
          name: "vue",
          var: "Vue",
          path: `https://unpkg.com/vue@3.4.21/dist/vue.global.js`,
         
        },
        {
          name: "vue-demi",
          var: "VueDemi",
          path: `https://unpkg.com/vue-demi@0.13.11`,
        },
        {
          name: "pinia",
          var: "Pinia",
          path: `https://unpkg.com/pinia@2.1.7/dist/pinia.iife.js`,
        },
        {
          name: "mitt",
          var: "mitt",
          path: `https://unpkg.com/mitt@3.0.1/dist/mitt.umd.js`,
        },
        {
          name: "echarts",
          var: "echarts",
          path: `https://unpkg.com/echarts@5.5.0/dist/echarts.js`,
        },
        {
          name: "axios",
          var: "axios",
          path: `https://unpkg.com/axios@1.6.8/dist/axios.min.js`,
        },

        {
          name: "crypto-js",
          var: "CryptoJs",
          path: `https://unpkg.com/crypto-js@4.2.0/index.js`,
        },
        {
          name: "md-editor-v3",
          var: "MdEditorV3",
          path: `https://unpkg.com/md-editor-v3@4.13.5/lib/umd/index.js`,
          css:'https://unpkg.com/md-editor-v3@4.16.9/lib/style.css'
        },
        
        {
          name: "vue-router",
          var: "VueRouter",
          path: `https://unpkg.com/vue-router@4.3.0`,
        },

        {
          name: "element-plus",
          var: "ElementPlus",
          path: "https://unpkg.com/element-plus@2.6.3/dist/index.full.js",
        },
        {
          name: '@element-plus/icons-vue',
          var: 'ElementPlusIconsVue', // 根据main.js中定义的来
          path: 'https://unpkg.com/@element-plus/icons-vue'
        },
        {
          name: "moment",
          var: "moment",
          path: `https://unpkg.com/moment@2.30.1/moment.js`,
        },
      ],
    }),
    compression({
      algorithm: "gzip", // 指定压缩算法为gzip,[ 'gzip' , 'brotliCompress' ,'deflate' , 'deflateRaw']
      ext: ".gz", // 指定压缩后的文件扩展名为.gz
      threshold: 1024, // 仅对文件大小大于threshold的文件进行压缩，默认为10KB
      deleteOriginFile: false, // 是否删除原始文件，默认为false
      filter: /\.(js|css|json|html|ico|svg)(\?.*)?$/i, // 匹配要压缩的文件的正则表达式，默认为匹配.js、.css、.json、.html、.ico和.svg文件
      compressionOptions: { level: 9 }, // 指定gzip压缩级别，默认为9（最高级别）
      verbose: true, //是否在控制台输出压缩结果
      disable: false, //是否禁用插件
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },

  css: {
    preprocessorOptions: {
      // 这里配置 mixin.scss 混合文件的全局引入
      scss: {
        additionalData: (content, loaderContext) => {
          if (loaderContext.endsWith("mixin.scss")) return content;
          return `@import "./src/assets/style/mixin.scss"; 
          ${content}`;
        },
      },
    },
  },
  server: {
    
      host: "0.0.0.0",
      port: 8008,
    
    proxy: {
      "/api": {
        target: "http://127.0.0.1",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
