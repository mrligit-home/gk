 <template>
  <Box_x
    :table="{
      gridOptions,
      watchData,
    }"
    :header="{ showList: [['Hform', 0, 1, 2, 3, 4]] }"
    @beforeClose="handleClose"
    @cancel="Cancel"
    @confirm="Confirm"
    @[code.SAVE]="ResetSearch"
  >
    <template #Tables="{ row }">
      <me_form
        v-bind="{ formConf, watchForm }"
        ref="ruleFormRef"
        :row="row"
      ></me_form>
    </template>
    <template #Hform="{ row }">
      <RegInput
        v-model="SearchForm.artictype"
        type="text"
        :size="row.size"
      ></RegInput>
    </template>
  </Box_x>
</template>
<script setup lang="ts">
// :header="{ showList: [['Hform', 0, 1, 2, 3, 4]] }"
// 动态展示header标签，位置顺序既数组顺序，二维数组，分别表示左右两边  Hform插槽传递即可
// import { VxeGridProps } from "vxe-table";
import { reactive, provide, toRefs, ref } from "vue";
import me_form from "@/components/Elcomponent/me_form.vue";  //表单组件
provide("size", { size: "small" });// 控制整体element 样式
import code from "@/util/code";  //执行代码指令

const formConf = {  //表单静态配置项
  disabled: false,   //整体禁用
  formItem: [        //formiten列表
    {
      formType: "me_input", //展示表单组件名， ItemConfig.children和 formType只能存在一个,
      required: true,       //必填
      trigger: "change",    //
      ItemConfig: {         //绑定在itemform一些配置
        label: "类型名称",
        prop: "User",
        valueFormate:['create']  //定义组件绑定给组件的数据类型，以及绑定的数据值，不写默认绑定prop对应的值
      },
      ComConfig:{//组件配置，具体参照element
      type: "date",  //
      },
       DataType: "url",  //结合ComConfig.type使用，判断生成的数据类型  图片：url  时间：time
    },
  ],
};
const watchForm = reactive({  //动态表单属性
  model: {},   //表单绑定值
  
  BaseConfig: {  //增删改查配置
    custom:true,
    [code.EDIT]: {
      Option: {
        url: "/qwe",
        method: "post",
      },
    },
    [code.ADD]: {
      Option: {
        url: "/qwe",
        method: "post",
      },
    },
  },
});
可以动态监听所有的header抛出的事件
let ResetSearch = () => {  
  console.log(123);
};
const ruleFormRef = ref();
const handleClose = () => {
  console.log(121);
};
const Cancel = (callback) => {  //取消
  ruleFormRef.value.reset();
  callback();
};
const Confirm = (callback) => {  //提交
  ruleFormRef.value.Form().then((res) => {
    console.log(73, res);
    callback();
  });
};

const gridOptions = {
  BaseConfig: {
    exportUrl: "",
    importUrl: "",
    exportMethod: "",
    importMethod: "",
    deleteConfig: {
      key: "delete_list",  //删除指定对象数据的key  {delete_list:[]}
      Option: { 
        url: "/qwe",
      },
    },
  },

  columns: [
    // { type: "sq", width: 60, label: "单选" },
    {
      prop: "title",
      label: "开关",
      custom：'插槽',  //插槽名称，slot失效
      slot: [  
        {
          VNode: "ElSwitch",
          model: "status", //绑定的值
          event: ["change"], //事件类型，只有change  click，更多徐自定义 custom：true
        },
      ],
    },
    {
      prop: "operate",
      label: "操作",
      width: 250,
      slot: [  
        //默认只有click，change，事件，其他暂不支持
        {
          VNode: "ElButton",  //组件名
          name: "修改",
          code: code.EDIT, //对应操作指令
          event: ["click"],
          config: {  //VNode配置
            type: "primary",  
          },
        },
      ],
    },
  ],
};
const watchData = reactive<any>({  //table 动态事件
  Option: {
    url: "/v1/articlist/all",
  },
  deleteConfig: {  删除回调
    callback: (e, call) => {
      call(e);
    },
  },
  SearchConfig: {  搜索回调
    callback: (e, call) => {
      console.log(2222);
      call(e);
    },
  },
  SearchForm: {   搜索配置
    artictype: "",
  },
});
const { SearchForm } = toRefs(watchData); //响应式解构
@param target 目标图片路径 不写默认使用prop参数
{
      prop: "cover",
      label: "封面",
      type: "image",
      slot: {
        target: "",
      },
    },
 */
</script>

 
